<?php

/**
 * @file
 * View template to display a list as a Google chart.
 */

/**
 * $options	          : View handler options
 * $graph_header      : Graph header
 * $graph_data        : Graph data array
 * $graph_options     : Graph options array
 * $graph_chart       : Graph chartapi calling variable.
 * $graph_id          : Graph DIV id
 * $callbackfunction  : Google set Callback function
 * $setonloadcallback : Google define Callback function
 * $row_classes	      : Define row classes
 * $rows	          : An array of row items. Each row is an array of content.
 */
?>
<script type="text/javascript">
  <?php print $setonloadcallback; ?>
    function <?php print $callbackfunction; ?> 
    {  
  <?php foreach($options['graph_type'] as $key => $val):
    {
      print $graph_header[$val] . $graph_data[$val] . $graph_options[$val] . $graph_chart[$val];
    }
  endforeach;
  ?>
    }
</script>
  <?php foreach($options['graph_type'] as $key => $val):
    {
      print '<div id="' . $graph_id[$val] . '" class="' . $graph_id[$val] . '"
      style="width: ' . $options['graph_width'] . '
      px;height:' . $options['graph_height'] . 'px;"></div>';
    }
  endforeach;
  ?>
