<?php

/**
 * @file
 * Contains the google_data_visualization style plugin . 
 */

/**
 * Style plugin to render each item in an ordered or unordered list . 
 *
 * @ingroup views_style_plugins
 */
class google_data_visualization_style_plugin extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['graph_type'] = array('default' => NULL);
    $options['graph_title'] = array('default' => NULL);
    $options['graph_width'] = array('default' => '500');
    $options['graph_height'] = array('default' => '400');
    $options['fontsize'] = array('default' => 12);
    $options['fontfamily'] = array('default' => 'thahoma');
    $options['vAxis'] = array('default' => '');
    $options['hAxis'] = array('default' => '');
    $options['graph_fields'] = array('default' => '');
    $options['easing'] = array('default' => NULL);
    $options['vertical'] = array('default' => FALSE);
    $options['navigation'] = array('default' => '');
    $options['ajax'] = array('default' => 0);
    $options['wrap'] = array('default' => NULL);
    $options['visible'] = array('default' => NULL);
    $options['skin'] = array('default' => NULL);
    $options['scroll'] = array('default' => '');
    return $options;
  }
  /**
   * Set default options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Build the list of skins as options.
    $skins = array();
    foreach (google_data_visualization_skins() as $key => $skin) {
      $skins[$key] = $skin['title'];
    }
    $skins[''] = t('None');
    $graph_fields = array();
    // Number of options to provide in count-based options.
    $range = drupal_map_assoc(range(1, 10));
    $auto_range = array('' => t('Auto')) + $range;

    $form['description'] = array(
      '#type' => 'markup',
      '#value' => '<div class="messages">' . t('The google_data_visualization style is affected by several other settings within the display .  The fist field from the view field option will set as "X-axix" of the chart . ') . '</div>',
    );

    $form['graph_type'] = array(
      '#type' => 'select',
      '#title' => t('Graph Type'),
      '#multiple' => TRUE,
      '#default_value' => $this->options['graph_type'],
      '#prefix' => '<div class="messages">' . t('The google_data_visualization style is affected by several other settings within the display .  The fist field from the view field option will set as "X-axix" of the chart . ') . '</div>',
      '#description' => t('Specifies whether to wrap at the first/last item (or both) and jump back to the start/end . '),
      '#options' => array(
        'AreaChart' => t('Area Chart'),
        'BarChart' => t('Bar Chart'),
        'BubbleChart' => t('Bubble Chart'),
        'ColumnChart' => t('Column Chart'),
        'ComboChart' => t('Combo Chart'),
        'Gauge' => t('Gauge'),
        'ImageAreaChart' => t('ImageAreaChart'),
        'ImageChart' => t('Image Chart'),
        'LineChart' => t('Line Chart'),
        'PieChart' => t('Pie Chart'),
        'ScatterChart' => t('Scatter Chart'),
        'ImageAreaChart' => t('ImageAreaChart'),
        'SteppedAreaChart' => t('Stepped Area Chart'),
        'Table' => t('Table Chart'),
      ),
    );
    $form['graph_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#size' => 50,
      '#maxlength' => 200,
      '#default_value' => $this->options['graph_title'],
      '#description' => t('Specifies font family . '),
    );
    $form['graph_width'] = array(
      '#type' => 'textfield',
      '#title' => t('Graph Width'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => $this->options['graph_width'],
      '#field_suffix' => ' '  .  t('px'),
      '#description' => t('Specifies the graph width . '),
    );
    $form['graph_height'] = array(
      '#type' => 'textfield',
      '#title' => t('Graph Height'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => $this->options['graph_height'],
      '#field_suffix' => ' '  .  t('px'),
      '#description' => t('Specifies the graph height . '),
    );
    $form['fontsize'] = array(
      '#type' => 'textfield',
      '#title' => t('Font Size'),
      '#size' => 4,
      '#maxlength' => 4,
      '#default_value' => $this->options['fontsize'],
      '#field_suffix' => ' '  .  t('px'),
      '#description' => t('Specifies the font size . '),
    );
    $form['fontfamily'] = array(
      '#type' => 'textfield',
      '#title' => t('Font Family'),
      '#size' => 20,
      '#maxlength' => 20,
      '#default_value' => $this->options['fontfamily'],
      '#description' => t('Specifies font family . '),
    );
    $form['ajax'] = array(
      '#type' => 'checkbox',
      '#title' => t('AJAX load pages'),
      '#default_value' => $this->options['ajax'],
      '#description' => t('The number of items set in the pager settings will be preloaded. All additional pages will be loaded by AJAX as needed . '),
    );
    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );
    $form['advanced']['hAxis'] = array(
      '#type' => 'textfield',
      '#title' => t('hAxis Title'),
      '#size' => 25,
      '#maxlength' => 200,
      '#default_value' => $this->options['hAxis'],
      '#description' => t('Specifies hAxis Title . '),
    );
    $form['advanced']['vAxis'] = array(
      '#type' => 'textfield',
      '#title' => t('vAxis Title'),
      '#size' => 20,
      '#maxlength' => 200,
      '#default_value' => $this->options['vAxis'],
      '#description' => t('Specifies vAxis Title . '),
    );
  }
  /**
   * Set default Validate function.
   */
  function validate() {
    $errors = parent::validate();
    if ($this->display->handler->use_pager()) {
      $errors[] = t('The google_data_visualization style cannot be used with a pager. Disable the "Use pager" option for this display.');
    }
    return $errors;
  }
}
