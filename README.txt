Installation
------------
1) Place this module directory in your modules folder (this will usually be
   "sites/all/modules/").

2) Enable the module within your Drupal site at Administer -> Site Building ->
   Modules (admin/build/modules).

Usage
-----
The Google Data Visualization module is most commonly used with the Views module 
to turn listings of datas into goolge charts.

1) Install the Views module (http://drupal.org/project/views) on your Drupal
   site if you have not already.

2) Add a new view at Administration -> Structure -> Views 
  (admin/structure/views).

3) Change the "Display format" of the view to "google_data_visualization". 
   Disable the"Use pager" option, which cannot be used with the 
   Google Data Visualization style. 
   Click the   "Continue & Edit" button to configure the rest of the View.

4) Click on the "Settings" link next to the google_data_visualization Format to 
   configure the options for the selecting different charts and for 
   set the chart variable.
